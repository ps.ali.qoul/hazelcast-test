# hazelcast-test

server config:

You have to add this map definition to the server configuration XML file (config/hazelcast.xml)

```xml
    <map name="participants">
        <in-memory-format>BINARY</in-memory-format>
        <metadata-policy>CREATE_ON_UPDATE</metadata-policy>
        <backup-count>1</backup-count>
        <async-backup-count>0</async-backup-count>
        <time-to-live-seconds>0</time-to-live-seconds>
        <max-idle-seconds>0</max-idle-seconds>
        <eviction eviction-policy="NONE" max-size-policy="PER_NODE" size="0"/>
        <merge-policy batch-size="100">com.hazelcast.spi.merge.PutIfAbsentMergePolicy</merge-policy>
        <cache-deserialized-values>INDEX-ONLY</cache-deserialized-values>
        <statistics-enabled>true</statistics-enabled>
        <per-entry-stats-enabled>false</per-entry-stats-enabled>

        <map-store enabled="true" initial-mode="LAZY">
            <class-name>com.progressoft.cache.store.cache.ParticipantMapLoader</class-name>
            <write-delay-seconds>0</write-delay-seconds>
            <write-batch-size>1000</write-batch-size>
            <write-coalescing>true</write-coalescing>
        </map-store>

    </map>
 ```

The cache-store project is packaged as fat jar and it includes the models jar as well. You will have to copy the jar
to the lib folder inside your hazelcast deployment so that it can load it at startup
