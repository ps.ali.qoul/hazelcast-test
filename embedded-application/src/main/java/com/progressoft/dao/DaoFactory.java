package com.progressoft.dao;

public interface DaoFactory {

    ParticipantDao makeParticipantDao();
}
