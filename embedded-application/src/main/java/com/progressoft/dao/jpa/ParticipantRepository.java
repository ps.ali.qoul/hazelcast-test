package com.progressoft.dao.jpa;

import com.progressoft.cache.store.jpa.ParticipantEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ParticipantRepository extends CrudRepository<ParticipantEntity, Long> {
    Optional<ParticipantEntity> findByCode(String code);

    @Override
    List<ParticipantEntity> findAll();
}
