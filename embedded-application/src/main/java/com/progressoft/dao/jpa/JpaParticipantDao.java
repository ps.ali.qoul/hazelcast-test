package com.progressoft.dao.jpa;

import com.progressoft.cache.store.jpa.ParticipantEntity;
import com.progressoft.dao.ParticipantDao;
import com.progressoft.models.Participant;

import java.util.stream.Collectors;

public class JpaParticipantDao implements ParticipantDao {

    private final ParticipantRepository participantRepository;

    public JpaParticipantDao(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @Override
    public Participant get(String code) {
        return participantRepository.findByCode(code)
                .map(this::createParticipant)
                .orElse(null);
    }

    @Override
    public Iterable<String> getAllCodes() {
        return participantRepository.findAll()
                .stream()
                .map(ParticipantEntity::getCode)
                .collect(Collectors.toList());
    }

    @Override
    public Long save(String code, Participant value) {
        ParticipantEntity entity = participantRepository.findByCode(code)
                .map(p -> {
                    p.setName(value.getName());
                    p.setBic(p.getBic());
                    return p;
                })
                .orElse(createParticipantEntity(value));
        entity = participantRepository.save(entity);
        return entity.getId();
    }

    private Participant createParticipant(ParticipantEntity p) {
        return new Participant(p.getId(), p.getCode(), p.getName(), p.getBic());
    }

    private ParticipantEntity createParticipantEntity(Participant participant) {
        ParticipantEntity result = new ParticipantEntity();
        result.setCode(participant.getCode());
        result.setBic(participant.getBic());
        result.setName(participant.getName());
        return result;
    }
}
