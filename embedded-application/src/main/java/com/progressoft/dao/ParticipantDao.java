package com.progressoft.dao;

import com.progressoft.models.Participant;

public interface ParticipantDao {

    Participant get(String code);

    Iterable<String> getAllCodes();

    Long save(String code, Participant value);
}
