package com.progressoft.cache;

import com.hazelcast.map.MapStore;
import com.progressoft.dao.ParticipantDao;
import com.progressoft.models.Participant;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParticipantMapLoader implements MapStore<String, Participant> {

    private final ParticipantDao participantDao;

    public ParticipantMapLoader(ParticipantDao participantDao) {
        this.participantDao = participantDao;
    }

    @Override
    public Participant load(String s) {
        System.out.println("loading entry for key: " + s);
        return participantDao.get(s);
    }

    @Override
    public Map<String, Participant> loadAll(Collection<String> collection) {
        return collection.stream()
                .collect(Collectors.toMap(Function.identity(), this::load));
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return participantDao.getAllCodes();
    }

    @Override
    public void store(String key, Participant value) {
        participantDao.save(key, value);
    }

    @Override
    public void storeAll(Map<String, Participant> map) {
        map.forEach(this::store);
    }

    @Override
    public void delete(String key) {
        // do nothing for now
    }

    @Override
    public void deleteAll(Collection<String> keys) {
        keys.forEach(this::delete);
    }
}
