package com.progressoft.platform.factory;

import com.progressoft.dao.DaoFactory;
import com.progressoft.dao.ParticipantDao;
import com.progressoft.dao.jpa.JpaParticipantDao;
import com.progressoft.dao.jpa.ParticipantRepository;
import org.springframework.stereotype.Component;

@Component
public class JpaDaoFactory implements DaoFactory {

    private final ParticipantRepository participantRepository;

    public JpaDaoFactory(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @Override
    public ParticipantDao makeParticipantDao() {
        return new JpaParticipantDao(participantRepository);
    }
}
