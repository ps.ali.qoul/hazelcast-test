package com.progressoft.platform;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.progressoft.dao.DaoFactory;
import com.progressoft.dao.ParticipantDao;
import com.progressoft.models.Participant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/cache/")
public class ParticipantResource {

    private final IMap<String, Participant> participantsMap;
    private final ParticipantDao dao;

    public ParticipantResource(HazelcastInstance hazelcastInstance, DaoFactory daoFactory) {
        participantsMap = hazelcastInstance.getMap("participants");
        dao = daoFactory.makeParticipantDao();
    }

    @GetMapping("/{key}")
    public Participant get(@PathVariable("key") String key) {
        return participantsMap.get(key);
    }

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody Participant participant) {
        participantsMap.put(participant.getCode(), participant);
        return ResponseEntity.created(URI.create("/" + participant.getCode())).build();
    }

    // The following two end points are used to test how fast can you write and read directly from the DB
    @GetMapping("/db/{key}")
    public Participant getFromDb(@PathVariable("key") String key) {
        return dao.get(key);
    }

    @PostMapping("/db/")
    public ResponseEntity<?> createToDb(@RequestBody Participant participant) {
        dao.save(participant.getCode(), participant);
        participantsMap.put(participant.getCode(), participant);
        return ResponseEntity.created(URI.create("/" + participant.getCode())).build();
    }
}