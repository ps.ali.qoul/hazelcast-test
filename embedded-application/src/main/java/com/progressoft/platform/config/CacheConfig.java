package com.progressoft.platform.config;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.progressoft.cache.store.cache.ParticipantMapLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class CacheConfig {

    private final HazelcastProperties hazelcastProperties;

    public CacheConfig(HazelcastProperties hazelcastProperties) {
        this.hazelcastProperties = hazelcastProperties;
    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        Config config = createConfig(hazelcastProperties);
        return Hazelcast.newHazelcastInstance(config);
    }

    private Config createConfig(HazelcastProperties hazelcastProperties) {
        Config config = new XmlConfigBuilder()
                .build();
        config.setNetworkConfig(networkConfig(config.getNetworkConfig(),
                List.of(hazelcastProperties.getMembers().split(",")), hazelcastProperties.getPort()));
        config.getMapConfig("participants").setMapStoreConfig(createParticipantStoreConfig());
        return config;
    }

    private NetworkConfig networkConfig(NetworkConfig networkConfig, List<String> members, int port) {
        networkConfig.setJoin(joinConfig(members));
        networkConfig.setPort(port);
        return networkConfig;
    }

    private JoinConfig joinConfig(List<String> members) {
        JoinConfig joinConfig = new JoinConfig();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getTcpIpConfig().setEnabled(true);
        joinConfig.getTcpIpConfig().setMembers(members);
        return joinConfig;
    }

    private MapStoreConfig createParticipantStoreConfig() {
        ParticipantMapLoader participantMapLoader = new ParticipantMapLoader();
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setImplementation(participantMapLoader);
        mapStoreConfig.setWriteDelaySeconds(0);
        return mapStoreConfig;
    }

}
