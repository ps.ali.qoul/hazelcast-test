package com.progressoft.cache.store.jpa;


import javax.persistence.*;

@Entity
@Table(name="participants", indexes = {@Index(name = "PARTICIPANTS_CODE_IDX", unique = true, columnList = "code")})
@NamedQuery(name="Participant_ParticipantByCode", query = "from ParticipantEntity where code=:code")
public class ParticipantEntity {

    @Id
    @GeneratedValue
    private long id;

    private String code;
    private String name;
    private String bic;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }
}
