package com.progressoft.cache.store;

import com.progressoft.cache.store.cache.ParticipantMapLoader;
import com.progressoft.models.Participant;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        ParticipantMapLoader loader = new ParticipantMapLoader();

        String code = UUID.randomUUID().toString();
        Participant participant = new Participant(0L, code, code, code);
        loader.store(code, participant);
        System.out.println(code);
    }
}
