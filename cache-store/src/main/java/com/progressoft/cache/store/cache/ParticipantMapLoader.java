package com.progressoft.cache.store.cache;

import com.hazelcast.map.MapStore;
import com.progressoft.cache.store.Database;
import com.progressoft.cache.store.jpa.ParticipantEntity;
import com.progressoft.models.Participant;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParticipantMapLoader implements MapStore<String, Participant> {

    @Override
    public Participant load(String code) {
        try (Session session = Database.getSessionFactory().openSession()) {
            return createParticipant(loadInternal(code, session));
        }
    }

    @Override
    public Map<String, Participant> loadAll(Collection<String> collection) {
        try (Session session = Database.getSessionFactory().openSession()) {
            return collection.stream()
                    .map(code -> loadInternal(code, session))
                    .map(this::createParticipant)
                    .collect(Collectors.toMap(Participant::getCode, Function.identity()));
        }
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return Collections.emptyList();
    }

    @Override
    public void store(String key, Participant value) {
        Session session = Database.getSessionFactory().openSession();
        executeInTransaction(() -> storeInternal(key, value, session), session);
    }

    @Override
    public void storeAll(Map<String, Participant> map) {
        Session session = Database.getSessionFactory().openSession();
        executeInTransaction(() -> map.forEach((k,v) -> storeInternal(k, v, session)), session);
    }

    @Override
    public void delete(String key) {
        // do nothing for now
    }

    @Override
    public void deleteAll(Collection<String> keys) {
        keys.forEach(this::delete);
    }

    private void executeInTransaction(Runnable runnable, Session session){
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            runnable.run();
            tx.commit();
        }catch (Exception e){
            if(tx != null && tx.isActive())
                tx.rollback();
        }
    }

    private void storeInternal(String key, Participant value, Session session){
        ParticipantEntity entity = Optional.ofNullable(loadInternal(key, session))
                .map(e -> {
                    e.setName(value.getName());
                    e.setBic(value.getBic());
                    return e;
                }).orElse(createParticipantEntity(value));
        session.merge(entity);
    }

    private ParticipantEntity loadInternal(String code, Session session) {
        return session.createNamedQuery("Participant_ParticipantByCode", ParticipantEntity.class)
                .setParameter("code", code)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    private Participant createParticipant(ParticipantEntity entity) {
        if(entity == null)
            return null;
        return new Participant(entity.getId(), entity.getCode(), entity.getName(), entity.getBic());
    }

    private ParticipantEntity createParticipantEntity(Participant participant) {
        ParticipantEntity result = new ParticipantEntity();
        result.setCode(participant.getCode());
        result.setBic(participant.getBic());
        result.setName(participant.getName());
        return result;
    }
}
