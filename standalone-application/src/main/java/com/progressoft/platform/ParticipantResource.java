package com.progressoft.platform;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.progressoft.models.Participant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/cache/")
public class ParticipantResource {

    private static final Logger log = LoggerFactory.getLogger(ParticipantResource.class);

    private final IMap<String, Participant> participantsMap;

    public ParticipantResource(HazelcastInstance hazelcastInstance) {
        participantsMap = hazelcastInstance.getMap("participants");
    }

    @GetMapping("/{key}")
    public Participant get(@PathVariable("key") String key) {
        return participantsMap.get(key);
    }

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody Participant participant) {
//        log.info("received record with key: {}", participant.getCode());
        participantsMap.put(participant.getCode(), participant);
        return ResponseEntity.created(URI.create("/" + participant.getCode())).build();
    }
}