package com.progressoft.platform.config;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class CacheConfig {

    @Bean
    public HazelcastInstance hazelcastInstance(@Value("${servers}") String members) {
        ClientConfig config = createConfig(members);
        return HazelcastClient.newHazelcastClient(config);
    }

    private ClientConfig createConfig(String members) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setClusterName("dev");
        System.out.println(members);
        clientConfig.getNetworkConfig()
                .setAddresses(List.of(members.split(",")));
        return clientConfig;
    }

}
