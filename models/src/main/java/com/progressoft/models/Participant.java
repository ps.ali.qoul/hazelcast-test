package com.progressoft.models;

import java.io.Serializable;

public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String code;
    private String name;
    private String bic;

    public Participant(Long id, String code, String name, String bic) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.bic = bic;
    }

    public Participant(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }
}
