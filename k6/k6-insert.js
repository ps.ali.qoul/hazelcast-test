import http from "k6/http";
import { check } from 'k6';
import { generateParticipant } from './participant.js';


export default function () {

    let anysize = 4;//the size of string
    let charset = "abcdefghijklmnopqrstuvwxyz";
    let headers = {'Content-Type': 'application/json'};
    let result="";
    for( let i=0; i < anysize; i++ )
        result += charset[Math.floor(Math.random() * charset.length)];

    let payload = JSON.stringify(generateParticipant(result));
    const response = http.post("http://localhost:8080/cache/", payload, {headers: headers});
    const checkRes = check(response, {
        'status is 201': r => r.status === 201
    });


}