import http from "k6/http";
import { check } from 'k6';
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';
import { generateParticipant } from './participant.js';


export default function () {

    const randomUUID = uuidv4();
    let headers = {'Content-Type': 'application/json'};
     let payload = JSON.stringify(generateParticipant(randomUUID));
    const response = http.post("http://localhost:8080/cache/", payload, {headers: headers});
    const checkRes = check(response, {
        'status is 201': r => r.status === 201
    });

}