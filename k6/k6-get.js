import http from "k6/http";
import { check } from 'k6';


export default function () {

    let keys = new Set()
    keys.add("AAA")
    keys.add("BBB")
    keys.add("CCC")

    function getRandomItem(set) {
        let items = Array.from(set);
        return items[Math.floor(Math.random() * items.length)];
    }

    const response = http.get("http://localhost:8080/cache/" + getRandomItem(keys));
    const checkRes = check(response, {
        'status is 200': r => r.status === 200
    });

}