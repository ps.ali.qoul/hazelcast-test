
export const generateParticipant = (token) => ({
    code: token,
    name: token,
    bic: token,
});